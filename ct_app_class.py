import sqlite3
from ct_trainer_engine import Trainer_engine
from sqlite3 import Error
from tkinter import *
import os
from collections import OrderedDict
import random
import matplotlib.pyplot as plt

AVERAGE_MOVES_PER_GAME = 38  # The average number of overall moves per game is around 38.
STARTER_USER_ID_VALUE = 1000

password_input = Entry
id_input = Entry
confirm_password_input = Entry

depth_level = Entry
aggressive_level = Entry
defensive_level = Entry
accuracy_level = Entry
is_debug_mode = Entry
is_user_start = Entry


class App:
    def __init__(self):
        self.open_user_id = -1
        self.ct_gui = Tk()
        try:
            self.sqliteConnection = sqlite3.connect('ct_project_SQLite.db')
            self.cursor = self.sqliteConnection.cursor()
            self.cursor.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='users' ''')

            if self.cursor.fetchone()[0] == 1:  # checking if db already exists
                return

            # Creating tables and setting up the environment for the sqlite database
            self.cursor.execute(''' CREATE TABLE users(id INTEGER PRIMARY KEY, username TEXT, password TEXT) ''')

            self.cursor.execute(''' CREATE TABLE user_statistics (overall_games INTEGER, overall_wins INTEGER, 
            overall_loses INTEGER, average_move_score REAL, overall_rating REAL, user_id INTEGER, 
            average_moves_per_win REAL, average_moves_per_lose REAL, FOREIGN KEY( user_id) REFERENCES users( id)) ''')

            self.cursor.execute('''CREATE TABLE pieces_statistics ( 
            
                                        pawn_loses INTEGER, pawn_wins INTEGER, 
                                        knight_loses INTEGER, knight_wins INTEGER,
                                        bishop_loses INTEGER, bishop_wins INTEGER,
                                        rook_loses INTEGER, rook_wins INTEGER,
                                        queen_loses INTEGER, queen_wins INTEGER,
                                        user_id INTEGER, FOREIGN KEY(user_id) REFERENCES users(id)) ''')

            self.cursor.execute('''CREATE TABLE single_game(
            game_id INTEGER PRIMARY KEY,
            game_score REAL, 
            current_rating REAL,
            user_aggressive_lvl REAL,
            opponent_depth_level INTEGER,
            user_defensive_lvl REAL,
            opponent_aggressive_lvl REAL,
            date TEXT,
            opponent_defensive_lvl REAL,           
            is_winner INTEGER,
            num_of_moves INTEGER,
            user_id INTEGER, FOREIGN KEY(user_id) REFERENCES users(id))''')

        except Error as error:
            print(error)

    def main_menu(self):
        self.ct_gui.destroy()

        self.ct_gui = Tk()  # This creates the window, just a blank one.
        self.ct_gui.title('Main Menu')  # This renames the title of said window to 'signup'
        title = Label(self.ct_gui,
                      text='[!] WELCOME TO MY CHESS TRAINER PROGRAM! [!]\nMade With Love By: Shalev Barshishat', )
        title.grid(row=0, column=3)

        button1 = button2 = button3 = button4 = button5 = button6 = button7 = button8 = button9 = button10 = Button()
        menu_buttons = {"Add User": [self.add_user, button1],  # done
                        "Open User": [self.open_user, button2],  # done
                        "Quick Match": [self.quick_match, button3],  # done
                        "Custom Match": [self.custom_match, button4],  # done
                        "View Users List": [self.print_users_list, button5],
                        "View User Statistics": [self.print_statistics, button6],  # done
                        "View User Progress": [self.print_progress, button7],
                        "View Top Users": [self.print_top_users, button8],
                        "Close User": [self.close_user, button9],  # done
                        "Save AND Exit": [self.close_program, button10]  # done
                        }

        for key, value in menu_buttons.items():
            menu_buttons[key][1] = Button(self.ct_gui, text=key, width=50, command=value[0])
            menu_buttons[key][1].grid(column=3, pady=5, padx=100)

        self.ct_gui.mainloop()

    def check_open_user(self):

        user_id_to_open = int(id_input.get())
        user_to_open_pass = str(password_input.get())
        try:
            sqlite_query = "SELECT password FROM users WHERE id= " + str(user_id_to_open)
            self.cursor.execute(sqlite_query)
            db_records = self.cursor.fetchall()
            if len(db_records) == 0:
                Label(self.ct_gui, text='Error! User with the id: ' + str(user_id_to_open) + ' not Found!\n',
                      foreground="Red").grid()
                return

            if db_records[0][0] != user_to_open_pass:
                Label(self.ct_gui,
                      text='Error! Wrong password for the user with the id: ' + str(user_id_to_open) + '\n',
                      foreground="Red").grid()
                return

            self.open_user_id = int(user_id_to_open)
            self.main_menu()

        except Error as error:
            print(error)

    def open_user(self):
        global id_input
        global password_input

        self.ct_gui.destroy()
        self.ct_gui = Tk()

        self.ct_gui.title('Chess Trainer - Open User')

        Label(self.ct_gui, text='Fill The Fields In Order To Open Your Chess Account: \n').grid()
        Label(self.ct_gui, text='[!] Enter User ID: ').grid(row=1, sticky=W)
        Label(self.ct_gui, text='[!] Enter User Password: ').grid(row=2, sticky=W)

        id_input = Entry(self.ct_gui)
        password_input = Entry(self.ct_gui, show='*')

        id_input.grid(row=1, column=1)
        password_input.grid(row=2, column=1)

        submit_button = Button(self.ct_gui, text='Open User', command=lambda: self.check_open_user())
        submit_button.grid(row=3, columnspan=2, sticky=W)
        submit_button = Button(self.ct_gui, text='Back To Menu', command=lambda: self.main_menu())
        submit_button.grid(row=3, columnspan=2, sticky=E)

    def close_user(self):
        if self.open_user_id == -1:  # checking if there are no open users
            Label(self.ct_gui, text='There is no open user to close!\n', foreground="Red").grid(column=3)
            return
        Label(self.ct_gui, text='Successfully close user with the id: ' + str(self.open_user_id) + '\n',
              foreground="24C8D5").grid(column=3)

        self.open_user_id = -1

    def add_user(self):
        global confirm_password_input
        global id_input
        global password_input

        self.ct_gui.destroy()
        self.ct_gui = Tk()

        self.ct_gui.title('Chess Trainer - Add User')

        Label(self.ct_gui, text='Fill The Fields In Order Create A Chess Account: \n').grid()
        Label(self.ct_gui, text='[!] Enter Username: ').grid(row=1, sticky=W)
        Label(self.ct_gui, text='[!] Enter Password: ').grid(row=2, sticky=W)
        Label(self.ct_gui, text='[!] ReEnter Password: ').grid(row=3, sticky=W)

        id_input = Entry(self.ct_gui)
        password_input = Entry(self.ct_gui, show='*')
        confirm_password_input = Entry(self.ct_gui, show='*')

        id_input.grid(row=1, column=1)
        password_input.grid(row=2, column=1)
        confirm_password_input.grid(row=3, column=1)

        submit_button = Button(self.ct_gui, text='Create User', command=lambda: self.check_add_user())
        submit_button.grid(row=4, columnspan=2, sticky=W)
        submit_button = Button(self.ct_gui, text='Back To Menu', command=lambda: self.main_menu())
        submit_button.grid(row=4, columnspan=2, sticky=E)

        self.sqliteConnection.commit()

    def check_add_user(self):
        try:
            new_username = id_input.get()
            new_password = password_input.get()

            if new_password != confirm_password_input.get():
                Label(self.ct_gui, text='Error! Password and Confirm Password are not match\n', foreground="Red").grid()
                return

            self.cursor.execute(''' SELECT COUNT(id) FROM users ''')
            db_records = self.cursor.fetchall()
            new_id = db_records[0][0] + 1 + STARTER_USER_ID_VALUE

            sqlite_query = " INSERT INTO users VALUES (" + str(new_id) + ",'" + str(new_username) + "','" + str(
                new_password) + "')"
            self.cursor.execute(sqlite_query)

            sqlite_query = " INSERT INTO user_statistics VALUES (0, 0, 0, 0, 0, " + str(new_id) + ", 0, 0)"
            self.cursor.execute(sqlite_query)
            sqlite_query = " INSERT INTO pieces_statistics VALUES (0, 0, 0, 0, 0, 0, 0, 0, 0, 0," + str(new_id) + ")"
            self.cursor.execute(sqlite_query)

            self.open_user_id = new_id
            # print("Successfully created a new user with id: ", new_id)
            self.main_menu()

        except Error as error:
            print(error)

    def print_statistics(self):
        if self.open_user_id == -1:
            Label(self.ct_gui, text='Error! You need to open a user in order to view user statistics!\n',
                  foreground="Red").grid(column=3)
            return

        self.ct_gui.destroy()
        self.ct_gui = Tk()

        self.ct_gui.title('Chess Trainer - User Statistics')

        sqlite_query = ''' SELECT * FROM user_statistics WHERE user_id = ''' + str(self.open_user_id)
        self.cursor.execute(sqlite_query)
        db_records_statistics = self.cursor.fetchall()

        sqlite_query = ''' SELECT * FROM pieces_statistics WHERE user_id = ''' + str(self.open_user_id)
        self.cursor.execute(sqlite_query)
        db_records_pieces_statistics = self.cursor.fetchall()

        Label(self.ct_gui, text="USER [%d] GAME STATISTIC" % self.open_user_id, font=("Arial", 10)).grid(sticky=W)

        user_game_statistics = [
            "[!] Overall Games Played:",
            "[!] Overall Wins: ",
            "[!] Overall Loses: ",
            "[!] Average Move score: ",
            "[!] OverAll rating: ",
            "[!] User ID: ",
            "[!] Average Moves per Win: ",
            "[!] Average Moves per Lose: ",
        ]

        parameters_num = len(db_records_statistics[0])
        row_to_start = 1
        for i in range(parameters_num):  # Rows
            Label(self.ct_gui, text=user_game_statistics[i]).grid(row=i + row_to_start, column=0, sticky=W)
            Label(self.ct_gui, text=str(db_records_statistics[0][i])).grid(row=i + row_to_start, column=1, sticky=W)

        row_to_start += parameters_num
        Label(self.ct_gui, text="USER'S PIECES STATISTICS", font=("Arial", 10)).grid(sticky=W)
        row_to_start += 1

        Label(self.ct_gui, text="Piece Type/ Piece Counter").grid(row=row_to_start, column=0, sticky=W)
        Label(self.ct_gui, text="Piece Loses").grid(row=row_to_start, column=1, sticky=W)
        Label(self.ct_gui, text="Piece Kills").grid(row=row_to_start, column=2, sticky=W)

        row_to_start += 1
        piece_type = ["[!] Pawn", "[!] Knight", "[!] Bishop", "[!] Rook", "[!] Queen"]
        for i in range(5):
            Label(self.ct_gui, text=piece_type[i]).grid(row=i + row_to_start, column=0, sticky=W)
            Label(self.ct_gui, text=str(db_records_pieces_statistics[0][i])).grid(row=i + row_to_start, column=1, sticky=W)
            Label(self.ct_gui, text=str(db_records_pieces_statistics[0][4 + i])).grid(row=i + row_to_start, column=2, sticky=W)

        Label(self.ct_gui, text="USER'S OVERALL LEVEL", font=("Arial", 10)).grid(row=row_to_start + 5,sticky=W)
        row_to_start += 6
        user_level = self.get_user_level()

        over_all = ["USER OVERALL RATING: ", "USER OVERALL AGGRESSIVE LEVEL [100 NORMAL]: ", "USER OVERALL DEFENSIVE LEVEL [100 NORMAL]: "]
        for i in range(len(user_level)):
            Label(self.ct_gui, text=over_all[i]).grid(row=row_to_start + i, column=0,sticky=W)
            Label(self.ct_gui, text=user_level[i]).grid(row=row_to_start + i, column=1,sticky=W)

    def get_user_level(self):

        user_stats = dict()

        sqlite_query = '''SELECT overall_rating,average_moves_per_win FROM user_statistics 
        WHERE user_id = ''' + str(self.open_user_id)
        self.cursor.execute(sqlite_query)
        db_user_level_info = self.cursor.fetchall()

        sqlite_query = ''' SELECT * FROM pieces_statistics WHERE user_id = ''' + str(self.open_user_id)
        self.cursor.execute(sqlite_query)
        db_records_pieces_statistics = self.cursor.fetchall()

        sum_user_kills_score = 0
        sum_user_loses_score = 0
        pieces_value = {0: 10, 1: 30, 2: 30, 3: 50, 4: 90}

        for i in range(5):
            sum_user_loses_score += db_records_pieces_statistics[0][i] * pieces_value[i]
            sum_user_kills_score += db_records_pieces_statistics[0][i + 1] * pieces_value[i]
        try:
            user_defensive_level = (100 * sum_user_kills_score) / sum_user_loses_score
            user_aggressive_level = (db_user_level_info[0][1] * 100) / AVERAGE_MOVES_PER_GAME
            user_aggressive_level = min(user_aggressive_level, 200)
            user_aggressive_level = 200 - user_aggressive_level
        except:
            user_defensive_level = 100
            user_aggressive_level = 100

        return [db_user_level_info[0][0], user_aggressive_level, user_defensive_level]

    def close_program(self):
        self.sqliteConnection.commit()
        self.sqliteConnection.close()
        sys.exit()

    def quick_match(self):
        if self.open_user_id == -1:  # checking if there are no open users
            Label(self.ct_gui, text='There is no open user to close!\n', foreground="Red").grid(column=3)
            return

        sqlite_query = '''SELECT average_move_score FROM user_statistics WHERE user_id = ''' + str(self.open_user_id)
        self.cursor.execute(sqlite_query)
        average_move_score = self.cursor.fetchall()

        user_level = self.get_user_level()
        try:
            trainer_depth = round(user_level[0] / (100 * average_move_score[0][0]))
        except:
            trainer_depth = 1
        if trainer_depth == 0:
            trainer_depth = 1
        print("trainer_depth", trainer_depth)
        is_starting = bool(random.getrandbits(1))  # getting random bit - 0 or 1 then converting to bool

        print("average_move_score", average_move_score[0][0], "user_level", user_level[1], " | ", user_level[2])
        chess_game = Trainer_engine(self.open_user_id)
        if average_move_score[0][0] == 0:
            chess_game.trainer_vs_user(trainer_depth, 100, is_starting, 100, 100)
        else:
            chess_game.trainer_vs_user(trainer_depth, average_move_score[0][0], is_starting, user_level[1],
                                       user_level[2])

        self.quick_match()
        self.main_menu()

    def custom_match(self):
        if self.open_user_id == -1:
            Label(self.ct_gui, text='Error! You need to open a user in order to view user statistics!\n',
                  foreground="Red").grid(column=3)
            return

        global depth_level
        global aggressive_level
        global defensive_level
        global accuracy_level
        global is_debug_mode
        global is_user_start

        self.ct_gui.destroy()
        self.ct_gui = Tk()

        self.ct_gui.title('Chess Trainer - Custom Game')

        Label(self.ct_gui, text='Fill The Fields In Order To Start A Custom Game: \n').grid()
        Label(self.ct_gui, text='[!] Enter Trainer Depth Level [1 - 7], '
                                'Warning! the bigger the depth the longer the time of thinking: ').grid(row=1, sticky=W)
        Label(self.ct_gui, text='[!] Enter Trainer Aggressive Level [0 - 200], 100 as Default: ').grid(row=2, sticky=W)
        Label(self.ct_gui, text='[!] Enter Trainer Defensive Level [0 - 200], 100 as Default: ').grid(row=3, sticky=W)
        Label(self.ct_gui, text='[!] Enter Trainer Accuracy Level [0 - 100]: ').grid(row=4, sticky=W)
        Label(self.ct_gui, text='[!] Who Should Start? 0 = Trainer else You!: ').grid(row=6, sticky=W)

        depth_level = Entry(self.ct_gui)
        aggressive_level = Entry(self.ct_gui)
        defensive_level = Entry(self.ct_gui)
        accuracy_level = Entry(self.ct_gui)
        is_user_start = Entry(self.ct_gui)

        depth_level.grid(row=1, column=1)
        aggressive_level.grid(row=2, column=1)
        defensive_level.grid(row=3, column=1)
        accuracy_level.grid(row=4, column=1)
        is_user_start.grid(row=6, column=1)

        submit_button = Button(self.ct_gui, text='Start Game', command=lambda: self.check_custom_game_fields())
        submit_button.grid(row=7, columnspan=2, sticky=W)
        submit_button = Button(self.ct_gui, text='Back To Menu', command=lambda: self.main_menu())
        submit_button.grid(row=7, columnspan=2, sticky=E)

    def check_custom_game_fields(self):
        if float(depth_level.get()) not in range(1, 8) or float(aggressive_level.get()) not in range(0, 201) \
                or float(defensive_level.get()) not in range(0, 201) or float(accuracy_level.get()) not in range(0,
                                                                                                                 101):
            Label(self.ct_gui, text='Error! Wrong Inputs\n', foreground="Red").grid(column=3)
            return

        self.ct_gui.destroy()
        chess_game = Trainer_engine(self.open_user_id)
        chess_game.trainer_vs_user(float(depth_level.get()), float(accuracy_level.get()), int(is_user_start.get()) != 0,
                                   float(aggressive_level.get()), float(defensive_level.get()))
        self.main_menu()

    def print_progress(self):  # will print user progress in score, defensive, aggressive and wins
        if self.open_user_id == -1:  # checking if there are no open users
            Label(self.ct_gui, text='There is no open user to close!\n', foreground="Red").grid(column=3)
            return

        sqlite_query = ''' SELECT * FROM single_game WHERE user_id = ''' + str(self.open_user_id)
        self.cursor.execute(sqlite_query)
        db_records_single_game = self.cursor.fetchall()

        x_values = []
        y_current_rating = []
        y_defensive_level = []
        y_aggressive_level = []
        y_is_winner = []

        for i in range(len(db_records_single_game)):
            x_values.append(i)
            y_current_rating.append(db_records_single_game[i][2])
            y_defensive_level.append(db_records_single_game[i][3])
            y_aggressive_level.append(db_records_single_game[i][4])
            y_is_winner.append(db_records_single_game[i][6])

        plt.plot(x_values, y_aggressive_level, label="aggressive level")
        plt.plot(x_values, y_defensive_level, label="defensive level")
        plt.plot(x_values, y_is_winner, label="is winner")
        plt.plot(x_values, y_current_rating, label="current rating")

        # naming the x axis
        plt.xlabel('Game dates')
        # naming the y axis
        plt.ylabel('Game statistics')
        # giving a title to my graph
        plt.title('USER[%d] GAME STATISTICS' % self.open_user_id)

        # show a legend on the plot
        plt.legend()

        # function to show the plot
        plt.show()

    def print_users_list(self):
        self.cursor.execute(''' SELECT id,username FROM users ''')
        users_list = self.cursor.fetchall()

        self.ct_gui.destroy()
        self.ct_gui = Tk()
        self.ct_gui.title('Chess Trainer - Users List')
        for user in users_list:
            Label(self.ct_gui, text=("[!] Username: " + str(user[1]) + " | User ID:" + str(user[0]))).grid(sticky=W)

        submit_button = Button(self.ct_gui, text='Back To Menu', command=lambda: self.main_menu())
        submit_button.grid(columnspan=2, sticky=E)

    def print_top_users(self):
        self.cursor.execute(''' SELECT id,username FROM users ''')
        users_list = self.cursor.fetchall()

        self.ct_gui.destroy()
        self.ct_gui = Tk()
        self.ct_gui.title('Chess Trainer - Users List')
        users_rating_dict = dict()
        for user in users_list:
            self.cursor.execute(" SELECT overall_rating FROM user_statistics WHERE user_id=" + str(user[0]))
            users_rating_dict[user] = self.cursor.fetchall()[0][0]

        sorted_users_rating_dict = sorted(users_rating_dict.items(), key=lambda kv: kv[1])
        sorted_users_rating_dict.reverse()

        for user in sorted_users_rating_dict:
            Label(self.ct_gui, text=("[!] Username: " + str(user[0][1]) + " | User ID: " + str(
                user[0][0]) + " | User Rating: " + str(user[1]))).grid(sticky=W)

        submit_button = Button(self.ct_gui, text='Back To Menu', command=lambda: self.main_menu())
        submit_button.grid(columnspan=2, sticky=E)
