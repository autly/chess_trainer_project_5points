import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the address given on the command line
server_address = ('127.0.0.1', 888)
print('starting up on ', server_address)
sock.bind(server_address)
sock.listen(1)

while True:
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('client connected:', client_address)
        while True:
            data = connection.recv(128)
            print('received "%s"' % data)
            if data:
                connection.sendall('received, thanks@'.encode())
            else:
                break
    finally:
        print('not good')
        connection.close()
