import chess
import sqlite3
from sqlite3 import Error
from tkinter import *
from datetime import datetime

STARTER_GAME_SCORE = 1
AVERAGE_MOVES_PER_GAME = 38  # The average number of overall moves per game is around 38.


class User:
    def __init__(self, id):
        self.id = id
        self.is_winner = False
        self.number_of_moves = 0
        self.list_enemy_pieces_lost = {"P": 8, "N": 2, "B": 2, "R": 2, "Q": 1}
        self.list_user_pieces_lost = {"P": 8, "N": 2, "B": 2, "R": 2, "Q": 1}
        self.moves_per_win = 0
        self.moves_per_lose = 0
        self.average_moves_score = 0
        self.current_game_rating = 0
        self.sqliteConnection = sqlite3.connect('ct_project_SQLite.db')
        self.cursor = self.sqliteConnection.cursor()

    def get_info_endgame(self, board, number_of_moves, is_winner, is_white, dict_user_score_per_move,
                         trainer_depth_level, trainer_accuracy_level):
        self.number_of_moves = number_of_moves
        self.is_winner = is_winner
        for y in range(8):  # running on all of the squares in board
            for x in range(8):
                piece = str(board.piece_at(8 * y + x))  # getting the piece that in the place

                if is_white is piece.isupper() and len(piece) == 1:  # checking if the piece is
                    # user's piece
                    try:
                        self.list_user_pieces_lost[piece.upper()] -= 1
                    except:
                        pass
                elif len(piece) == 1:  # means the piece is owned by enemy
                    try:
                        self.list_enemy_pieces_lost[piece.upper()] -= 1
                    except:
                        pass
        if self.is_winner:
            self.current_game_rating = STARTER_GAME_SCORE * trainer_depth_level * trainer_accuracy_level / 10
        else:
            self.current_game_rating = - STARTER_GAME_SCORE * trainer_depth_level * trainer_accuracy_level / 10

        count = 0
        for key, value in dict_user_score_per_move.items():
            count += 1

        self.average_moves_score = sum(dict_user_score_per_move.values()) / count

    def print_game_result(self):

        end_game_results = Tk()
        end_game_results.title('Chess Trainer - End Game Results')

        Label(end_game_results, text='----====[EndGame Results]====----\n').grid()
        Label(end_game_results, text='[!] is winner - ' + str(self.is_winner)).grid()
        Label(end_game_results, text='[!] id - ' + str(self.id)).grid()
        Label(end_game_results, text='[!] number of moves - ' + str(self.number_of_moves)).grid()
        Label(end_game_results, text='[!] list enemy lost - ' + str(self.list_enemy_pieces_lost)).grid()
        Label(end_game_results, text='[!] list user lost - ' + str(self.list_user_pieces_lost)).grid()

        submit_button = Button(end_game_results, text='Back To Menu', command=lambda: end_game_results.destroy())
        submit_button.grid(row=4, columnspan=2, sticky=E)

        end_game_results.mainloop()

    def insert_data_to_sqlite(self):
        sqlite_query = ''' SELECT * FROM user_statistics WHERE user_id = ''' + str(self.id)
        self.cursor.execute(sqlite_query)
        records_statistics = self.cursor.fetchall()

        sqlite_query = ''' SELECT * FROM pieces_statistics WHERE user_id = ''' + str(self.id)
        self.cursor.execute(sqlite_query)
        records_pieces_statistics = self.cursor.fetchall()

        sqlite_query = "UPDATE user_statistics set overall_games = " + str(
            records_statistics[0][0] + 1) + " WHERE user_id = " + str(self.id)
        self.cursor.execute(sqlite_query)

        if self.is_winner:
            sqlite_query = "UPDATE user_statistics set overall_wins = " + str(
                records_statistics[0][1] + 1) + " WHERE user_id = " + str(self.id)
        else:
            sqlite_query = "UPDATE user_statistics set overall_loses = " + str(
                records_statistics[0][2] + 1) + " WHERE user_id = " + str(self.id)

        self.cursor.execute(sqlite_query)

        average_move_score = (records_statistics[0][3] * records_statistics[0][0] + self.average_moves_score) / (
                records_statistics[0][0] + 1)

        sqlite_query = "UPDATE user_statistics set average_move_score = " + str(
            average_move_score) + " WHERE user_id = " + str(self.id)
        self.cursor.execute(sqlite_query)

        sqlite_query = "UPDATE user_statistics set overall_rating = " + str(
            records_statistics[0][4] + self.current_game_rating) + " WHERE user_id = " + str(self.id)
        self.cursor.execute(sqlite_query)

        if self.is_winner:
            average_moves_per_win = (records_statistics[0][6] * records_statistics[0][1] + self.number_of_moves) / (
                    records_statistics[0][1] + 1)

            sqlite_query = "UPDATE user_statistics set average_moves_per_win = " + str(
                average_moves_per_win) + " WHERE user_id = " + str(self.id)
        else:
            average_moves_per_lose = (records_statistics[0][7] * records_statistics[0][
                2] + self.number_of_moves) / (records_statistics[0][2] + 1)

            sqlite_query = "UPDATE user_statistics set average_moves_per_lose = " + str(
                average_moves_per_lose) + " WHERE user_id = " + str(self.id)
        self.cursor.execute(sqlite_query)

        sqlite_query = "UPDATE pieces_statistics set " + \
                       "pawn_loses = " + str(records_pieces_statistics[0][0] + self.list_user_pieces_lost["P"]) + \
                       ", pawn_wins = " + str(records_pieces_statistics[0][1] + self.list_enemy_pieces_lost["P"]) + \
                       ", knight_loses = " + str(records_pieces_statistics[0][2] + self.list_user_pieces_lost["N"]) + \
                       ", knight_wins = " + str(records_pieces_statistics[0][3] + self.list_enemy_pieces_lost["N"]) + \
                       ", bishop_loses = " + str(records_pieces_statistics[0][4] + self.list_user_pieces_lost["B"]) + \
                       ", bishop_wins = " + str(records_pieces_statistics[0][5] + self.list_enemy_pieces_lost["B"]) + \
                       ", rook_loses = " + str(records_pieces_statistics[0][6] + self.list_user_pieces_lost["R"]) + \
                       ", rook_wins = " + str(records_pieces_statistics[0][7] + self.list_enemy_pieces_lost["R"]) + \
                       ", queen_loses = " + str(records_pieces_statistics[0][8] + self.list_user_pieces_lost["Q"]) + \
                       ", queen_wins = " + str(records_pieces_statistics[0][9] + self.list_enemy_pieces_lost["Q"]) + \
                       " WHERE user_id = " + str(self.id)
        self.cursor.execute(sqlite_query)

        self.sqliteConnection.commit()

        self.cursor.execute("select count(*) from single_game")
        overall_num_of_games = self.cursor.fetchall()

        current_rating = records_pieces_statistics[0][4]
        user_current_level = self.get_user_current_level()
        now = datetime.now()
        current_time = now.strftime("%d/%m/%Y %H:%M:%S")

        sqlite_query = " INSERT INTO single_game VALUES (" \
                        + str(overall_num_of_games[0][0] + 1) + "," \
                                                            + str(self.current_game_rating) + "," + \
                       str(current_rating) + "," \
                                              + str(user_current_level[1]) + "," + str(user_current_level[0]) + ',"' + \
                       str(current_time) + '","' + str(self.is_winner) + '",'+ str(self.number_of_moves) + ',' + str(self.id) + ")"

        self.cursor.execute(sqlite_query)

        self.sqliteConnection.commit()
        self.sqliteConnection.close()

    def get_user_current_level(self):
        sqlite_query = '''SELECT average_moves_per_win FROM user_statistics 
        WHERE user_id = ''' + str(self.id)
        self.cursor.execute(sqlite_query)
        db_user_level_info = self.cursor.fetchall()

        sqlite_query = ''' SELECT * FROM pieces_statistics WHERE user_id = ''' + str(self.id)
        self.cursor.execute(sqlite_query)
        db_records_pieces_statistics = self.cursor.fetchall()

        sum_user_kills_score = 0
        sum_user_loses_score = 0
        pieces_value = {0: 10, 1: 30, 2: 30, 3: 50, 4: 90}

        for i in range(5):
            sum_user_loses_score += db_records_pieces_statistics[0][i] * pieces_value[i]
            sum_user_kills_score += db_records_pieces_statistics[0][i + 1] * pieces_value[i]

        try:
            user_defensive_level = (100 * sum_user_kills_score) / sum_user_loses_score
            user_aggressive_level = (db_user_level_info[0][1] * 100) / AVERAGE_MOVES_PER_GAME
            user_aggressive_level = min(user_aggressive_level, 200)
            user_aggressive_level = 200 - user_aggressive_level
        except:
            user_defensive_level = 100
            user_aggressive_level = 100
        return [user_aggressive_level, user_defensive_level]
