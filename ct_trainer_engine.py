import chess
import math
import random
import sys
import time
from ct_piece_scores import *
from ct_user_class import User
from tkinter import *
from PIL import ImageTk, Image
import operator

HIGHEST_STARTING_SCORE = 9999
LOWEST_STARTING_SCORE = -9999
positions_evaluated = 0
WHITE_TURN_MIN_MAX = True
dict_user_move_scores = dict()
is_user_color_white = True
TRAINER_AGGRESSIVE_LEVEL = 0
TRAINER_DEFENSIVE_LEVEL = 0
label_pieces_array = dict()
user_move_choice_input = ""
global_root = 0
global_feedback = 0
global_trainer_thinking = 0
has_button_pressed = False


def button_pressed():
    global has_button_pressed
    has_button_pressed = True


class Trainer_engine:
    def __init__(self, user_id):
        self.bot_type = 0
        self.board_gui = None
        self.user_id = user_id
        self.board = chess.Board()
        self.user_move_choice = ""

    def miniMaxRoot(self, depth, isMaximizing, trainer_accuracy):
        global positions_evaluated
        global WHITE_TURN_MIN_MAX
        positions_evaluated = 0

        possibleMoves = self.board.legal_moves  # will get a list of all the possible moves
        start = time.time()
        dict_moves = dict()

        highest_move_score = LOWEST_STARTING_SCORE
        lowest_move_score = HIGHEST_STARTING_SCORE
        print("===={ Trainer Process Thinking: }====")
        print("[!] Possible Moves for 1 depth: ", possibleMoves)

        if depth % 2 == 0:
            WHITE_TURN_MIN_MAX = not WHITE_TURN_MIN_MAX

        for x in possibleMoves:  # running on all of the possible moves
            move = chess.Move.from_uci(str(x))
            self.board.push(move)  # updating the self.board with the new move
            current_move_score = self.miniMax(depth - 1, not isMaximizing, -10000,
                                              10000)  # will get a score for the move and

            highest_move_score = max(highest_move_score,
                                     current_move_score)  # getting the current max highest move score
            lowest_move_score = min(lowest_move_score,
                                    current_move_score)  # getting the current max highest move score)
            self.board.pop()  # undoing the move from the self.board

            dict_moves[move] = current_move_score  # adding the current move and score into a dict that contain all of

            # the data of the trainer optional moves
        dict_best_possible_moves = dict()
        chosen_move = 0

        difference = abs(lowest_move_score - highest_move_score)

        for key, value in dict_moves.items():
            if abs(lowest_move_score - value) >= difference * trainer_accuracy / 100:
                dict_best_possible_moves[key] = value

        chosen_move = random.choice(list(dict_best_possible_moves.keys()))
        end = time.time()
        print("[!] All Moves + Score: ", dict_moves)
        print("[!] Best Possible Moves: ", dict_best_possible_moves)
        print("[!] Chosen Move: ", chosen_move)
        print("[!] Chosen Move SCORE: ", dict_moves[chosen_move])
        print("[!] Thinking time: ", end - start)
        print("[!] Positions evaluated: ", positions_evaluated)
        print("================================================")

        return chosen_move, end - start

    def miniMax(self, depth, is_maximizing, alpha, beta):
        global WHITE_TURN_MIN_MAX
        global positions_evaluated

        positions_evaluated += 1
        if depth == 0:
            ans = self.evaluation()
            # print(" WHITE_TURN_MIN_MAX: ", WHITE_TURN_MIN_MAX)

            # print(ans)
            return ans

        possibleMoves = self.board.legal_moves
        if is_maximizing:
            best_move_score = -9999
            for x in possibleMoves:
                move = chess.Move.from_uci(str(x))
                self.board.push(move)
                best_move_score = max(best_move_score, self.miniMax(depth - 1, not is_maximizing, alpha, beta))
                self.board.pop()

                alpha = max(alpha, best_move_score)
                if beta <= alpha:
                    return best_move_score
            return best_move_score
        else:
            best_move_score = 9999
            for x in possibleMoves:
                move = chess.Move.from_uci(str(x))
                self.board.push(move)
                best_move_score = min(best_move_score, self.miniMax(depth - 1, not is_maximizing, alpha, beta))
                self.board.pop()

                beta = min(beta, best_move_score)
                if beta <= alpha:
                    return best_move_score
            return best_move_score

    def calc_player_move_score(self, depth, isMaximizing, chosen_move):
        global dict_user_move_scores
        global has_button_pressed
        global WHITE_TURN_MIN_MAX

        has_button_pressed = False

        possibleMoves = self.board.legal_moves  # will get a list of all the possible moves
        dict_moves = dict()
        highest_move_score = LOWEST_STARTING_SCORE
        lowest_move_score = HIGHEST_STARTING_SCORE

        if depth % 2 == 0:
            WHITE_TURN_MIN_MAX = not WHITE_TURN_MIN_MAX

        for x in possibleMoves:  # running on all of the possible moves
            move = chess.Move.from_uci(str(x))
            self.board.push(move)  # updating the self.board with the new move
            current_move_score = self.miniMax(depth - 1, not isMaximizing, -10000,
                                              10000)  # will get a score for the move and

            highest_move_score = max(highest_move_score,
                                     current_move_score)  # getting the current max highest move score
            lowest_move_score = min(lowest_move_score, current_move_score)
            self.board.pop()  # undoing the move from the self.board

            dict_moves[move] = current_move_score  # adding the current move and score into a dict that contain all of
            # the data of the trainer optional moves

        user_move_score = dict_moves[chosen_move]
        user_move_final_score = 100

        if highest_move_score != lowest_move_score:
            user_move_final_score = abs(lowest_move_score - user_move_score) * 100 / abs(lowest_move_score - highest_move_score)

        dict_user_move_scores[str(chosen_move)] = user_move_final_score

        self.board_gui.title('Chess Trainer - User Move FeedBack')
        Label(self.board_gui, text='===={ Calculating User Move: }====\n', foreground="#054747").grid(column=1,
                                                                                                      sticky=W)

        Label(self.board_gui, text="[!] User move choice:  " + str(chosen_move), foreground="#018888").grid(column=1,
                                                                                                            sticky=W)

        Label(global_root, text="[!] Best possible Score: " + str(highest_move_score) + " ,Move: " + str(
            list(dict_moves.keys())[list(dict_moves.values()).index(highest_move_score)]), foreground="#018888").grid(
            column=1, sticky=W)
        Label(self.board_gui, text="[!]  Worst possible Score: " + str(lowest_move_score) + " ,Move: " + str(
            list(dict_moves.keys())[list(dict_moves.values()).index(lowest_move_score)]), foreground="#018888").grid(
            column=1, sticky=W)
        Label(self.board_gui, text="[!] User move choice:  " + str(chosen_move), foreground="#018888").grid(column=1,
                                                                                                            sticky=W)
        Label(self.board_gui, text="[!] User move choice:  " + str(chosen_move), foreground="#018888").grid(column=1,
                                                                                                            sticky=W)
        Label(self.board_gui,
              text="[!] User's Move Score: " + str(user_move_final_score)[:4] + "/ 100",
              foreground="#06412d", font=("Courier", 10)).grid(column=1, sticky=W)

        submit_button = Button(self.board_gui, text='Next Turn', command=lambda: button_pressed())
        submit_button.grid(row=2, column=1, sticky=E)

        # while not has_button_pressed:
        #    self.board_gui.update_idletasks()
        #    self.board_gui.update()

    def evaluation(self):
        """
            function will check each square and will get the value of its piece, will summaries the score and return it,
            the less horrible the score is the better score.
        """
        score = 0
        for y in range(8):  # running on all of the squares in self.board
            for x in range(8):
                score += getPieceValue(str(self.board.piece_at(8 * y + x)), y, x, WHITE_TURN_MIN_MAX,
                                       self.bot_type, TRAINER_AGGRESSIVE_LEVEL,
                                       TRAINER_DEFENSIVE_LEVEL)  # getting piece score value
        return score

    def check_valid_move(self, move):
        if len(move) != 4:
            print("[{!}] Error! move needs to be 4 chars long ")
            return False
        if 97 <= ord(move[0]) <= 104 and 49 <= ord(move[1]) <= 56 and 97 <= ord(move[2]) <= 104 and 49 <= ord(
                move[3]) <= 56:
            if (move[0] is move[2]) and (move[1] is move[3]):
                print("[{!}] Error! can't move to the same square")
                return False
            possibleMoves = self.board.legal_moves
            move = chess.Move.from_uci(str(move))
            if move in possibleMoves:
                return True
        print("[{!}] Error! invalid move!")
        return False

    def get_move_from_user(self, is_wrong_input):
        global label_pieces_array
        global user_move_choice_input
        global global_root
        global has_button_pressed

        has_button_pressed = False

        Label(self.board_gui, text="Chess Game, Trainer VS User[%d]\n\n" % self.user_id, font=("Arial", 20)).grid(row=0)
        self.board_gui.geometry("800x500")
        self.tkinter_print_board()

        if is_wrong_input:
            Label(self.board_gui, text='[!] Error You Have Entered An Invalid Move!').grid(row=3, column=1)

        Label(self.board_gui, text='[!] Enter Your Next Move: ').grid(row=0, column=0, sticky=E)
        user_move_choice_input = Entry(self.board_gui)
        user_move_choice_input.grid(row=0, column=1, sticky=W)

        submit_button = Button(self.board_gui, text='Apply Move', command=lambda: button_pressed())
        submit_button.grid(row=2, column=1, sticky=W)

        while not has_button_pressed:
            self.board_gui.update_idletasks()
            self.board_gui.update()

        self.user_move_choice = (str(user_move_choice_input.get()) + '.')[:-1]

    def trainer_vs_user(self, trainer_depth, trainer_accuracy, is_white, trainer_aggressive_level,
                        trainer_defensive_level):
        global WHITE_TURN_MIN_MAX
        global is_user_color_white
        global TRAINER_AGGRESSIVE_LEVEL
        global TRAINER_DEFENSIVE_LEVEL
        global global_root
        TRAINER_AGGRESSIVE_LEVEL = trainer_aggressive_level
        TRAINER_DEFENSIVE_LEVEL = trainer_defensive_level

        label_pieces_array.clear()
        global_root = Tk()
        global_root.withdraw()
        self.board_gui = Toplevel(global_root)
        self.board_gui.title('Chess Trainer - BOARD')

        self.board = chess.Board()  # creating a self.board sample using chess library

        num_of_turns = 1
        if is_white:
            num_of_turns = 0

        is_user_color_white = is_white

        is_white_turn = True
        while not self.board.is_game_over():
            print(self.board)
            if num_of_turns % 2 == 0:  # checking if user turn
                wrong_input = False
                '''
                while True:  # running until player will enter a valid move
                    # TODO: code a listen line that will receive moves from player using GUI

                    self.get_move_from_user(wrong_input)
                    if self.check_valid_move(self.user_move_choice):  # checking if move is valid
                        move = chess.Move.from_uci(
                            str(self.user_move_choice))  # converting the player's move into a valid syntax for
                        # future use
                        break
                    for widget in self.board_gui.winfo_children():
                        widget.destroy()

                    wrong_input = True

                    while not has_button_pressed:
                        self.board_gui.update_idletasks()
                        self.board_gui.update()

                    for widget in self.board_gui.winfo_children():
                        widget.destroy()
                '''
                WHITE_TURN_MIN_MAX = is_white_turn
                self.bot_type = 2
                move, thinking_time = self.miniMaxRoot(3, True, 85)
                WHITE_TURN_MIN_MAX = is_white_turn
                self.bot_type = 0
                self.calc_player_move_score(3, True, move)  # function will get return user move score
            else:  # trainer turn
                self.bot_type = 1
                move, thinking_time = self.miniMaxRoot(trainer_depth, True, trainer_accuracy)
                WHITE_TURN_MIN_MAX = is_white_turn
                move = chess.Move.from_uci(str(move))  # converting the trainer's move into a valid syntax for future
                # use in chess library
                for widget in self.board_gui.winfo_children():
                    widget.destroy()
                self.tkinter_display_trainer_process(move, thinking_time)

            self.board.push(move)  # adding the move into our board
            is_white_turn = not is_white_turn
            WHITE_TURN_MIN_MAX = is_white_turn
            num_of_turns += 1

        if not is_white:
            num_of_turns -= 1

        is_winner = False
        if self.board.result() == "0-1" and not is_white:
            is_winner = True
        elif self.board.result() == "1-0" and is_white:
            is_winner = True

        print(self.board.result())
        print(is_winner)
        user = User(self.user_id)
        user.get_info_endgame(self.board, num_of_turns, is_winner, is_white, dict_user_move_scores, trainer_depth,
                              trainer_accuracy)
        user.insert_data_to_sqlite()
        # user.print_game_result()

    def tkinter_print_board(self):
        global label_pieces_array

        pieces_images_paths = {"e_b": "ct_pieces_img/empty_black.png",
                               "e_w": "ct_pieces_img/empty_white.png",
                               "P": "ct_pieces_img/p_white.png",
                               "N": "ct_pieces_img/n_white.png",
                               "B": "ct_pieces_img/b_white.png",
                               "R": "ct_pieces_img/r_white.png",
                               "Q": "ct_pieces_img/q_white.png",
                               "K": "ct_pieces_img/k_white.png",
                               "p": "ct_pieces_img/p_black.png",
                               "n": "ct_pieces_img/n_black.png",
                               "b": "ct_pieces_img/b_black.png",
                               "r": "ct_pieces_img/r_black.png",
                               "q": "ct_pieces_img/q_black.png",
                               "k": "ct_pieces_img/k_black.png"}

        locations_images_paths_rows = ["ct_pieces_img/1.png", "ct_pieces_img/2.png",
                                       "ct_pieces_img/3.png", "ct_pieces_img/4.png",
                                       "ct_pieces_img/5.png", "ct_pieces_img/6.png",
                                       "ct_pieces_img/7.png", "ct_pieces_img/8.png"]

        locations_images_paths_columns = ["ct_pieces_img/empty_white.png", "ct_pieces_img/A.png", "ct_pieces_img/B.png",
                                          "ct_pieces_img/C.png", "ct_pieces_img/D.png",
                                          "ct_pieces_img/E.png", "ct_pieces_img/F.png",
                                          "ct_pieces_img/G.png", "ct_pieces_img/H.png", "ct_pieces_img/empty_white.png"]

        reading_position_array = [7, -1, -1, 0, 9, 1, 0, 8, 1]
        if not is_user_color_white:
            reading_position_array = [0, 8, 1, 9, 0, -1, 7, -1, -1]

        label_pieces_array.clear()
        for row in range(reading_position_array[0], reading_position_array[1],
                         reading_position_array[2]):  # running on all of the squares in self.board
            img_location_path = locations_images_paths_rows[row]
            img_location = Image.open(img_location_path)
            img_location = img_location.resize((32, 32), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(image=img_location)
            label_pieces_array[img] = img_location

            for column in range(reading_position_array[6], reading_position_array[7], reading_position_array[8]):
                key = str(self.board.piece_at(8 * row + column))
                if key not in pieces_images_paths:
                    if (row + 1) % 2 == 0:
                        if column % 2 == 0:
                            img_piece_path = pieces_images_paths["e_w"]
                        else:
                            img_piece_path = pieces_images_paths["e_b"]
                    elif column % 2 == 0:
                        img_piece_path = pieces_images_paths["e_b"]
                    else:
                        img_piece_path = pieces_images_paths["e_w"]
                else:
                    img_piece_path = pieces_images_paths[key]
                img_piece = Image.open(img_piece_path)
                img_piece = img_piece.resize((32, 32), Image.ANTIALIAS)
                img = ImageTk.PhotoImage(image=img_piece)

                label_pieces_array[img] = img_piece

        for column in range(reading_position_array[3], reading_position_array[4], reading_position_array[5]):
            img_location_path = locations_images_paths_columns[column]
            img_location = Image.open(img_location_path)
            img_location = img_location.resize((32, 32), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(image=img_location)
            label_pieces_array[img] = img_location

        counter = 0

        for k, v in label_pieces_array.items():
            Label(self.board_gui, image=k).place(x=int(counter % 9) * 40, y=int(counter / 9) * 40 + 100,
                                                 width=v.size[0],
                                                 height=v.size[1])

            counter += 1

    def tkinter_display_trainer_process(self, chosen_move, process_time):
        Label(self.board_gui, text='===={ Trainer Process Of Thinking: }====\n', foreground="#781625").grid(column=1,
                                                                                                            sticky=W,
                                                                                                            row=10)
        Label(self.board_gui, text="[!] Chosen Move:  " + str(chosen_move),
              foreground="#E82240").grid(
            column=1, sticky=W, row=11)
        Label(self.board_gui, text="[!] Thinking time:  " + str(process_time),
              foreground="#E82240").grid(
            column=1, sticky=W, row=12)
        Label(self.board_gui, text="[!] Positions evaluated:  " + str(positions_evaluated),
              foreground="#E82240").grid(
            column=1, sticky=W, row=13)
